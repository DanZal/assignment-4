# English to American Sign Language (ASL) translator webapp made in React. 

# How to:

--------------  01 ----------------
1. create the project with the command **npx create-react-app "app-name"** in powershell
2. then cd into your app **cd "appname"**
3. Then install bootstrap with **npm install bootstrap@next**
4. Then imported google fonts from https://developers.google.com/fonts/docs/material_icons
5. in the root folder typing command: **code .** will open the project in VS.
6. in public folder in index.html pasted the <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet"> inside the head tag. For Google fonts.
7. In powershell install the rest of needed packages: **npm install react-router-dom redux react-redux date-fns**

--------------  02 ----------------
1. in the root folder run the command: **npm start** to test start the app
2. In index.js imported bootstrap wiht import 'bootstrap/dist/css/bootstrap.min.css'

--------------  03 ----------------
1. verify that all the dependencies are there: the react-redux, react-router etc the dependencies are listed in package.json file.
2. add browser router in app to enable routing
3. In app.js add import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom'
4. Wrap/put every html/jsx code inside the <BrowserRouter> tag
5. Added this in App.js   <Switch>
        <Route path="/" exact component={ Login } />
      </Switch>
6. created new folder components/login/ then Login.js inside this folder
7. Imported the newly created Login component in App.js
8. By default if you try to go to a route that does not exist for ex localhos:8080/about the website will render the global header and maybe footer but the stuff
inside the <Switch> because we have not defined a route for that.
9. So inorder to enusre that user dont navigate to a route that dont exist we will create a new component called NotFound and that Route in App.js with <Route path="*" component={Notfound}/>

--------------  04 ----------------
1. Created new folder hoc (higher order component) inside the folder create file AppContainer.js
2. Anything wrapped inside the <Appcontainer> will become the children for this component and have the defined style.

--------------  05 ----------------
1. Redux setup. command in powershell: **npm i redux-devtools-extension**
2. Setting up the store for redux.
3. reducers are where we tell the store what the new state will look like
4. middleware can run async code and intercepts
5. inside index.js in the src folder we setup our store, we create a <provider> and shift our <app> inside the provider
6. inside the provider tag we import or define the store by store= {store} since the store file is named index.js we dont need to specfy the file inside the import
7. inspect->redux-> you should see @@INIT if everthing is done correctly

--------------  06 ----------------
1. created three new files to store session which is the profile payload we get back after successfull login
the three files: sessionActions, sessionReducer, sessionMiddleware
2. the newly func inside sessionMiddleware must be decalred inside middleware/index.js
3. same goes with reducer the new reducer file/function needs to be dfined insidse /reducers/index.js
4. After a sucessful login we can now test this by doing inspect-Application-Localstorage and we should see the profile session being set
also in inpect-Redux-state(tab) we should see the sessionReducer
5. we can now use this state to redirect to a new page


# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
