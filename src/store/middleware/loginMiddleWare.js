import { LoginAPI } from "../../components/Login/LoginAPI"
import { ACTION_LOGIN_ATTEMPTING, ACTION_LOGIN_SUCCESS, loginErrorAction, loginSuccessAction } from "../actions/loginActions"
import { sessionSetAction } from "../actions/sessionActions"

export const loginMiddleware = ({dispatch}) => next => action => {
    
    next(action)

    if (action.type === ACTION_LOGIN_ATTEMPTING) {
        //the credentials the login function takes as argument
        //are part of the action payload
        LoginAPI.login(action.payload)
        .then(profile => {
            //Login_success
            dispatch( loginSuccessAction(profile))
        })
        .catch(error => {
            //ERROR ->
            dispatch(loginErrorAction(error.message))
        })
    }

    if (action.type === ACTION_LOGIN_SUCCESS) {
        //THe login_succes_action has the profile as payload
        //in the sessionMiddleware we set the session to the profile
        dispatch(sessionSetAction(action.payload))
    }

}