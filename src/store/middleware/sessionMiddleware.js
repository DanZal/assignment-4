import { ACTION_SESSION_INIT, ACTION_SESSION_SET, sessionSetAction } from "../actions/sessionActions"

export const sessionMiddleware = ({ dispatch }) => next => action => {
    next(action)


    if(action.type === ACTION_SESSION_INIT) {
        //read local storage
        const storedSession = localStorage.getItem('rtxtf-ss')
        //check if exist
        if(!storedSession) {
            return
        }
        const session = JSON.parse(storedSession)
        dispatch(sessionSetAction(session))
    }

    if(action.type === ACTION_SESSION_SET) {
        //storing session in local storage
        //localstorage can only store strings and not objects thats why we use stringfy
        localStorage.setItem('rtxtf-ss', JSON.stringify(action.payload))
    }
}