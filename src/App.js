import './App.css';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom'
import Login from './components/Login/Login'
import NotFound from './components/NotFound/NotFound';
import AppContainer from './hoc/AppContainer';
import Translator from './components/Translator/Translator';
import Register from './components/Register/Register';
 
function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <AppContainer>
        <h1 className="mb-5">Lost in Translation</h1>
        </AppContainer>
   

      <Switch>
        <Route path="/" exact component={ Login } />
        <Route path="/register" exact component={ Register } />
        <Route path="/translator" component= { Translator } />
        <Route path="*" component={ NotFound } />
      </Switch>
    </div>
    </BrowserRouter>

  );
}

export default App;
