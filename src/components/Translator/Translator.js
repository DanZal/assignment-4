import AppContainer from "../../hoc/AppContainer"

let toggled = false;

const Translator = () => {

    const showImg = event => {
        event.preventDefault() //stop the page reload.
        toggled = true;
        console.log("button clicked toggled")

    }

 
    return (
        <AppContainer>

            <h1>Translator</h1>

            <div className="mb-3">
                <label htmlFor="translate" className="form-label">Enter text to translate</label>
                 <input id="translate" type="text" placeholder="Hello World" className="form-control" variant="outlined"/>
            </div>

            <div className="mb-3">
                <label htmlFor="translate" className="form-label">Ouput/Translation</label>
                <output id="translation" type="text" placeholder="output"  className="form-control"/>
            </div>

            <div>
      <button className="btn btn-success btn-lg" onClick={showImg}>Translate</button>

    </div>
        </AppContainer>
    )
}
export default Translator