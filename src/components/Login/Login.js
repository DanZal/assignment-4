import { zhCN } from "date-fns/locale"
import { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Link, Redirect } from "react-router-dom"
import AppContainer from "../../hoc/AppContainer"
import { loginAttemptAction } from "../../store/actions/loginActions"

const Login = () => {

    //hooks
    const dispatch = useDispatch()
    const { loginError, loginAttempting } = useSelector(state => state.loginReducer)
    //loggIn is variable from sessionReducer
    const { loggedIn } = useSelector(state => state.sessionReducer)


    const [credentials, setCredentials] = useState({
        username: '',
        password: ''

    })

    const onInputChange = event => {
        //the event that we get here is html event
        setCredentials({
            ...credentials,
            //traget id is the id defined in hte input tag
            //also we only need one beacuse the id username and password are identically typed in the credentials useState() and for id in the input tags
            [event.target.id]: event.target.value
        })
    }

    const onFormSubmit = event => {
        event.preventDefault() //stop the page reload.
        dispatch(loginAttemptAction(credentials))

    }

    return (

        <>
            {loggedIn && <Redirect to="/timeline" />}
            {!loggedIn &&

                <AppContainer>
                    <form className="mt-3 mb-3" onSubmit={onFormSubmit}>

                        <h2>Login</h2>

                        <div className="mb-3">
                            <label htmlFor="username" className="form-label">Username</label>
                            <input id="username" type="text" placeholder="Enter your username" className="form-control" onChange={onInputChange} />
                        </div>

                        <div className="mb-3">
                            <label htmlFor="password" className="form-label">Password</label>
                            <input id="password" type="password" placeholder="Enter your password" className="form-control" onChange={onInputChange} />
                        </div>

                        <button type="submit" className="btn btn-primary btn-lg">Login</button>


                    </form>
                    {loginAttempting &&
                        <p>Trying to login...</p>
                    }


                    {loginError &&
                        <div className="alert alert-danger" role="alert">
                            <h4>Unsuccessful</h4>
                            <p className="mb-0"> {loginError}</p>
                        </div>
                    }

                    <p className="mb-3">
                        <Link to="/register">No account? Register here</Link>
                    </p>

                </AppContainer>

            }

        </>



    )

}

export default Login