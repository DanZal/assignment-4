export const LoginAPI = {
    login(credentials) {
        return fetch('https://noroff-react-txt-forum-api.herokuapp.com/users/login', {
        method: 'POST',    
        headers: {
                'Content-Type': 'application/json'
            },
        body: JSON.stringify(credentials)
        })
        .then(async (response) => {
            if(!response.ok) {
                const {error = 'An unkwon error occurred'} = await response.json()
                throw new Error(error) //Force it into catch
            }
            return response.json()
        })
    
    }
}